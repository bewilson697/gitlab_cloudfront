terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.43.0"
    }
  }
}



provider "aws" {
   
  region  = "us-east-1"
}

locals {
  simple-website_filepath = "../simple-website"
}

data "aws_s3_bucket" "selected" {
  bucket = "bwbook-manual-upload"
}

resource "aws_s3_bucket_policy" "read_bwbook" {
  bucket = data.aws_s3_bucket.selected.id
  policy = data.aws_iam_policy_document.read_bwbook_bucket.json
}

/*resource "aws_s3_bucket_object" "object" {
  depends_on = [data.aws_s3_bucket.selected]  
  for_each = fileset(local.simple-website_filepath, "**")  
  bucket = data.aws_s3_bucket.selected.bucket
  key    = each.key
  source = "${local.simple-website_filepath}/${each.value}"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("${local.simple-website_filepath}/${each.value}")
}*/




###################################
# CloudFront Origin Access Identity
###################################
resource "aws_cloudfront_origin_access_identity" "bwbook" {
  comment = "bwbook"
}




###################################
# IAM Policy Document
###################################
data "aws_iam_policy_document" "read_bwbook_bucket" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${data.aws_s3_bucket.selected.arn}/*"]   

   principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.bwbook.iam_arn]
    }
  }

 
}



###################################
#  LambdaFunction
###################################
resource "aws_iam_role" "lambda_role" {
 name   = "terraform_aws_lambda_role"
 assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
                    "edgelambda.amazonaws.com",
                    "lambda.amazonaws.com"
                ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# IAM policy for logging from a lambda

resource "aws_iam_policy" "iam_policy_for_lambda" {

  name         = "aws_iam_policy_for_terraform_aws_lambda_role"
  path         = "/"
  description  = "AWS IAM Policy for managing aws lambda role"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# Policy Attachment on the role.

resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role" {
  role        = aws_iam_role.lambda_role.name
  policy_arn  = aws_iam_policy.iam_policy_for_lambda.arn
}

# Generates an archive from content, a file, or a directory of files.

data "archive_file" "zip_the_python_code" {
 type        = "zip"
 source_dir  = "${path.module}/python/"
 output_path = "${path.module}/python/s3-website.zip"
}

# Create a lambda function
# In terraform ${path.module} is the current directory.

/*resource "random_string" "test" {
  length = 3
  lower  = true
}*/







resource "aws_lambda_function" "terraform_lambda_func" {
 filename                       = "${path.module}/python/s3-website.zip"
 function_name                  = "bw-Lambda-Function" # -${random_string.test.id}
 role                           = aws_iam_role.lambda_role.arn
 handler                        = "s3-website.lambda_handler"
 runtime                        = "python3.8"
 publish                        = true
 depends_on                     = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}


output "teraform_aws_role_output" {
 value = aws_iam_role.lambda_role.name
}

output "teraform_aws_role_arn_output" {
 value = aws_iam_role.lambda_role.arn
}

output "teraform_logging_arn_output" {
 value = aws_iam_policy.iam_policy_for_lambda.arn
}






###################################
# CloudFront
###################################


resource "aws_cloudfront_distribution" "bwbook" {
  enabled             = true
  comment =  timestamp()
default_root_object = "index.html"
  

  default_cache_behavior {
    allowed_methods        = [ "GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "bwbook-s3-origin"
    viewer_protocol_policy = "allow-all"
    compress               = true

    min_ttl     = 0
    default_ttl = 5 * 60
    max_ttl     = 60 * 60

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    lambda_function_association { 
        event_type = "viewer-request" 
        lambda_arn = "arn:aws:lambda:us-east-1:640149056592:function:bw-Lambda-Function:1"
    } 
  }



  origin {
    domain_name = "bwbook-manual-upload.s3.amazonaws.com"
    origin_id   = "bwbook-s3-origin"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.bwbook.cloudfront_access_identity_path
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate { 
        cloudfront_default_certificate = true 
    } 
}


